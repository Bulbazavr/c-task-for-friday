#include <iostream> //cout, cin

class String {
private:
    char* data_;
    int size_;
public:

    char* r_data(){
        return data_;
    }
    int r_size() const{
        return size_;
    }

    String() {
        data_ = new char[0];
        size_ = 0;
    }

    String(char *str) {
        int i = 0;
        while (str[i] != '\0') {
            i++;
        }
        data_ = new char[i];
        size_ = i;
        for (int j = 0; j < size_; j++) {
            data_[j] = str[j];
        }
    }

    String(char* str, int size) { //заполняет строку длины size имволом str
        size_ = size;
        data_ = new char[size];
        for (int i = 0; i < size + 1; ++i) {
            data_[i] = str[i];
        }
    }

    explicit String(int size) { //заполняет строку длины size случайными цыфрами 0-9
        char arr[size];
        int num = 0 + rand() % (9 - 0 + 1);
        for (int i = 0; i < size + 1; ++i) {
            arr[i] = num;
        }
    }

    ~String() {
        delete[] data_;
        std::cout << "роизошла диструкция" << std::endl;
    }

    void ToUpper() { //делает символы Up
        for (int i = 0; i < size_; i++) {
            data_[i] = (data_[i] - 32);
        }
    }

    void ToLower() {//делает символы Low
        for (int i = 0; i < size_; i++) {
            data_[i] = (data_[i] - 32);
        }
    }

    String(String &old){
        size_ = old.r_size();
        data_ = new char[size_];
        char* str = old.r_data();
        for(int i = 0; i < size_; i++){
            data_[i] = str[i];
        }
    }

    String& operator = (String s){
        delete[] data_;
        size_ = s.r_size();
        data_ = new char[size_];
        char* str = s.r_data();
        for(int i = 0; i < size_; i++){
            data_[i] = str[i];
        }
        return *this;
    }

    char& operator[] (int i) {
        return data_[i];
    }

    String& operator += (String s){
        char* new_s = new char[size_+s.r_size()];
        for(int i = 0; i < size_; i++){
            new_s[i] = s.r_data()[i];
        }
        for(int i = size_; i < size_+s.r_size(); i++){
            new_s[i] = data_[i];
        }
        delete[] data_;
        size_ += s.r_size();
        data_ = new char[size_];
        for(int i = 0; i < size_; i++){
            data_[i] = new_s[i];
        }
        delete [] new_s;
        return *this;
    }
};

String& operator + (String s1, String s2){
    char* new_s = new char[s1.r_size()+s2.r_size()];
    for(int i = 0; i < s1.r_size(); i++){
        new_s[i] = s1.r_data()[i];
    }
    for(int i = s1.r_size(); i < s1.r_size()+s2.r_size(); i++){
        new_s[i] = s2.r_data()[i];
    }
    int n = s1.r_size()+s2.r_size();
    String s(new_s, n);
    delete[] new_s;
    return s;
}

bool operator > (String s1, String s2){
    std::string st1 = s1.r_data();
    std::string st2 = s2.r_data();
    return st1 > st2;
}
bool operator < (String s1, String s2){
    std::string st1 = s1.r_data();
    std::string st2 = s2.r_data();
    return st1 < st2;
}
bool operator >= (String s1, String s2){
    std::string st1 = s1.r_data();
    std::string st2 = s2.r_data();
    return st1 >= st2;
}
bool operator <= (String s1, String s2){
    std::string st1 = s1.r_data();
    std::string st2 = s2.r_data();
    return st1 <= st2;
}
bool operator == (String s1, String s2){
    std::string st1 = s1.r_data();
    std::string st2 = s2.r_data();
    return st1 == st2;
}
bool operator != (String s1, String s2){
    std::string st1 = s1.r_data();
    std::string st2 = s2.r_data();
    return st1 != st2;
}


    int main() {
        char* BBoD = new char[11];
        BBoD[0] = 'l';BBoD[1] = 'u';BBoD[2] = 'b';BBoD[3] = 'i';BBoD[4] = 'm';
        BBoD[5] = 'i';BBoD[6] = 'y';BBoD[7] = 'C';BBoD[8] = '+';BBoD[9] = '+';
        BBoD[10] = '\0';
        String s1(BBoD);
        std::cout << s1.r_size() << ' ' << s1.r_data()[0] << std::endl;
        String s2("HELLO",5);
        std::cout << s2.r_size() << ' ' << s2.r_data()[0] << std::endl;
        String s3(11);
        std::cout << s3.r_size() << ' ' << s3.r_data()[0] << std::endl;
        s1.ToUpper();
        std::cout << s1.r_size() << ' ' << s1.r_data()[0] << std::endl;
        s2.ToLower();
        std::cout << s2.r_size() << ' ' << s2.r_data()[0] << std::endl;
        String s4(s2);
        std::cout << s4.r_size() << ' ' << s4.r_data()[0] << std::endl;
        s4 = s1;
        std::cout << s4.r_size() << ' ' << s4.r_data()[0] << std::endl;
        std::cout << s4[0] << std::endl;//[]
        s4 += s2;//+=
        std::cout << s4.r_size() << ' ' << s4.r_data()[0] << std::endl;
        bool a,b;//
        a = s1>s2;
        b = s1<s2;
        std::cout << a << ' ' << b << std::endl;
        return 0;
    }
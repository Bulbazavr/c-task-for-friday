#include <iostream>

int main() {
    int size;
    int sum;
    int final;
    int step;
    std::cin >> step;
    std::cin >> size;

    double *arr = new double[size]; //new[]

    for (int i = 1; i < size; step+i) {
        std::cin >> arr[i];
        std::cout << arr[i] << '\t';
    }
    for (int f = 2; f < size; f+step) {
        sum += arr[f];
    }
    final = sum / size;
    std::printf("%d", final);
    std::cout << std::endl; // delete []
    delete[] arr;
    return 0;
}
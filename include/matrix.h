#pragma once
#include <iostream>

class Matrix {
public:
    Matrix();

    Matrix(int rows, int cols);

    Matrix(int size);
//getters
    int rows() const;
    int cols() const;

    //setters
    Matrix& setRows(int rows);
    Matrix& setCols(int cols);

    ~Matrix();

private:
    int** data_ = nullptr;
    int rows_ =0;
    int cols_ =0;
};
#include <matrix.h>
#include <iostream>
int main(){
Matrix matrix(3);

std::cout << matrix.rows() <<'\n';
    return 0;
}

//definition
Matrix::Matrix() : data_(nullptr), rows_(0), cols_(0) {
    std::cout << "default ctor \n";
}

int Matrix::rows() const {
    return rows_;
}

int Matrix::cols() const {
    return rows_;
}

Matrix &Matrix::setRows(int rows) {
    rows_ = rows;
    return *this;
}

Matrix &Matrix::setCols(int cols) {
    cols_ = cols;
    return *this;
}

Matrix::~Matrix() {
    std:: cout << "ditor \n";
    for (int i = 0; i < rows_ ; ++i) {
        delete[] data_[i];
    }
    delete[] data_;
}

Matrix::Matrix(int size) : Matrix(size,size){
    std::cout << "1-agrs ctor \n";
}

Matrix::Matrix(int rows, int cols) {
    std::cout << "2-agrs ctor \n";
    rows_ = rows;
    cols_ = cols;
    data_ = new int *[rows];
    for (int i = 0; i < rows; ++i) {
        data_[i] = new int[cols];
    }
}

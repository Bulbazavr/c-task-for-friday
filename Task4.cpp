#include <iostream>

int func(int f){
    if(f>1){
        return f* func(f-1);
    }
}
int main(){
    int fact;
    int result =1;
    std::cin >>fact;
    if(fact > 50){
        std::cout << "number is too big";
    } else {
        for (int i = 1; i <= fact; ++i) {
            result*= i;
        }
        printf("iterative %d \n",result);
        printf("recursion%d \n", func(fact));
    }
}
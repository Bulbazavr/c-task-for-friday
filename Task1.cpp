#include <iostream> //cout, cin
#include <cmath> //abs
#include <string>

int main() {

    //1. Считывание данных
    std::string name;
    std::string surname;

    int age;
    int points;

    std::cout << "Enter data ... \n";
    std::cin >> name >> surname >> age >> points;
    int gg = 100 - age;
    // Вывод
    std::printf( "Chance to get %d points by %s %s equals %d %%" ,points,name.data() , surname.data(), gg);

    return 0;
}

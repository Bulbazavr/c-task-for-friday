#include <iostream>

void func(int &A,int &B){
    int I = A;
    A = B;
    B = I;
}
int main(){
    int A;
    int B;
    int Aa ;
    std::cin>>A >> B;
    std::printf("%d and %d \n",A,B);
    std::printf("%p and %p \n",&A,&B);
    func(A,B);
    std::printf("%d and %d \n",A,B);
    std::printf("%p and %p \n",&A,&B);
}

#include <cstdlib>

class SortStrategy
{
public:
    virtual  int* sort(int* arr,int size) = 0;
    virtual  ~SortStrategy() = 0;
    static void swap(int* xp, int* yp)
    {
        int temp = *xp;
        *xp = *yp;
        *yp = temp;
    }
};
class BubbleSortStrategy:SortStrategy
{
public:
    int* sort(int* arr,int size) override{
        int i, j;
        for (i = 0; i < size - 1; i++){
            for (j = 0; j < size - i - 1; j++){
                if (arr[j] > arr[j + 1]) {
                    swap(&arr[j], &arr[j + 1]);
                }
            }
        }
        return arr;
    }
};
class InsertionSortStrategy:SortStrategy
{
public:
    int* sort(int* arr,int size) override{
        int i, key, j;
        for (i = 1; i < size; i++)
        {
            key = arr[i];
            j = i - 1;
            while (j >= 0 && arr[j] > key)
            {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
        return arr;
    }
};

class SelectionSortStrategy:SortStrategy
{
public:
    int* sort(int arr[], int n) override{
        int i, j, min_idx;
        for (i = 0; i < n-1; i++)
        {
            min_idx = i;
            for (j = i+1; j < n; j++)
                if (arr[j] < arr[min_idx])
                    min_idx = j;
            if(min_idx!=i)
                swap(&arr[min_idx], &arr[i]);
        }
        return arr;
    }
};
class BogoSortStrategy:SortStrategy
{
public:
    int* sort(int arr[], int size) override
    {
        bool flag = false;
        int count = 0;
        int randSave = 0;
        while (!flag) {
            for (int i = 0; i < size; i++) {
                randSave = rand() % size;
                count = arr[i];
                arr [i] = arr[randSave];
                arr [randSave] = arr[count];
            }
            flag = true;
            while (--size > 0){
                if (arr[size] < arr[size - 1]) {
                    flag = false;
                }
            }
        }
        return arr;
    }

};

class Sort{
private:
    SortStrategy *sortStrategy;
public:
    Sort();
    void setStrategy(SortStrategy *strategy)
    {
        sortStrategy = strategy;
    }

    int* execSort(int*arr,int size){
        return sortStrategy->sort(arr,size);
    }
    ~Sort(){
        sortStrategy = nullptr;
    }
};